# README #

This repository is the home of some of the projects that I've worked on during my academic career.

### Project List ###

* **CardChallenge**: CardChallenge is a C# project written in Visual Studio Community 2017 for Mac v7.0.1 and has also been tested in Visual Studio 2015 and 2017 for Windows. [(Free version of VS2017)](https://www.visualstudio.com/vs/community/). To run this project, simply open in Visual Studio and run the CardChallenge.sln file. Please feel free to run the unit tests as well - the testing framework for this project is NUnit v3.6.1. Here are a few things to note:
    * Although this project is currently set for a standard deck of cards, it is extendable to other types of cards, by changing the CardSuit and CardValue enums to different values. If the CardValue enums are changed, please preserve the starting int value of one.  
    * The primary data structures used for this project are Lists. Lists were used for this project, as they give flexibility and efficiency with indexing and access.
    * The main sort algorithm is a bucket sort to sort suits, then an insertion sort to sort the values of each 'bucket'. Although insertion sort is a relatively simple sort, it was chosen for readability and its efficiency on small data sets.
    * The main sort algorithm sorts first by suit, then by numeric faceValue, in this case starting at Ace. This order was chosen because it is how a standard deck of cards is sorted from most manufacturers.

* **ChangeCalculator.java:** Refactored Haskell code to Java while preserving side effect free benefits.

More projects to be added soon --
MCIS - Mobile catering inventory system. MCIS is an iOS app developed using XCode and Swift.

### Who do I talk to? ###

* Sarah Alvarado, sarahjalvarado@gmail.com