﻿/******************************************************************************
 * Program: Card Challenge
 * Class:   CardChallengeTests
 * Author:  Sarah Alvarado
 * Date:    May 26, 2017
 * About:   The CardChallengeTests class houses the unit tests for this project.
 *          The testing framework used is NUnit.
 ******************************************************************************/
using NUnit.Framework;
using System;
using System.Collections.Generic;
using CardChallenge;

namespace CardChallenge.Tests
{
	[TestFixture]
	public class CardChallengeTests
	{
		private List<Card> testList;  //A list to test  

		/// <summary>
		/// Sets up a small list to test so that results can be easily verified
		/// and tests are independent of program.
		/// </summary>
		[SetUp]
		public void SetUp()
		{
			testList = new List<Card>{
				new Card(){suit=CardSuit.Heart,faceValue=CardValue.Ace},
				new Card(){suit=CardSuit.Heart,faceValue=CardValue.Jack},
				new Card(){suit=CardSuit.Spade,faceValue=CardValue.Two},
				new Card(){suit=CardSuit.Spade,faceValue=CardValue.Queen},
				new Card(){suit=CardSuit.Club,faceValue=CardValue.Three},
				new Card(){suit=CardSuit.Club,faceValue=CardValue.King},
				new Card(){suit=CardSuit.Diamond,faceValue=CardValue.Ten},
				new Card(){suit=CardSuit.Diamond,faceValue=CardValue.Nine},
			};
		}
		//BuildDeck() tests----------------------------------------------------
		/// <summary>
        /// Tests the build deck not null.
        /// </summary>
        [Test] 
		public void TestBuildDeckNotNull()
		{
			//Arrange
			DeckManipulator deckMan = new DeckManipulator();

			//Act
			List<Card> deck = deckMan.BuildDeck();

			//Assert
			Assert.NotNull(deck);

		}
        /// <summary>
        /// Tests the size of the build deck correct.
        /// </summary>
		[Test]
		public void TestBuildDeckCorrectSize()
		{
			//Arrange
			DeckManipulator deckMan = new DeckManipulator();

			//Act
			List<Card> deck = deckMan.BuildDeck();

			//Assert
			Assert.AreEqual((Enum.GetNames(typeof(CardSuit)).Length * Enum.GetNames(typeof(CardValue)).Length), deck.Count);
		}
		//ShuffledDeck() tests--------------------------------------------------
		/// <summary>
        /// Tests the deck is shuffled and not in the original order that the
        /// list given was in.
        /// </summary>
        [Test] 
		public void TestDeckShuffled()
		{
			//Arrange
			DeckManipulator deckMan = new DeckManipulator();

			//Act
			List<Card> shuffled = new List<Card>(testList);
			deckMan.ShuffleDeck(shuffled);

			//Assert
			Assert.AreNotEqual(testList, shuffled);
		}
        /// <summary>
        /// Tests the shuffle when the size of the list is 1.
        /// </summary>
		[Test] 
		public void TestShuffleSize1()
		{
			//Arrange
			DeckManipulator deckMan = new DeckManipulator();

			//Act
			List<Card> listOfOne = new List<Card> { new Card() { suit = CardSuit.Heart, faceValue = CardValue.Ace, } };
			List<Card> shuffled = new List<Card>(listOfOne);
			deckMan.ShuffleDeck(shuffled);

			//Assert
			Assert.AreEqual(listOfOne, shuffled);
		}
        /// <summary>
        /// Tests the shuffled empty list returns and empty list.> 
        /// </summary>
		[Test]
		public void TestShuffledEmptyList()
		{
			//Arrange
			DeckManipulator deckMan = new DeckManipulator();

			//Act
			List<Card> emptyList = new List<Card>();

			//Assert
			Assert.IsEmpty(deckMan.ShuffleDeck(emptyList));

		}
        /// <summary>
        /// Tests that the method throws a NullReferenceException with the appropriate
        /// message when the param being passed in is null.
        /// </summary>
		[Test] 
		public void TestShuffledNullList()
		{
			//Arrange
			DeckManipulator deckMan = new DeckManipulator();

			//Act
			List<Card> nullList = null;

			//Assert
			var exception = Assert.Throws<NullReferenceException>(() => deckMan.ShuffleDeck(nullList));
			Assert.That(exception.Message.Contains("Error: Cannot shuffle a null object"));

		}
		//SortedDeck() tests--------------------------------------------------
		/// <summary>
        /// Tests the sorted deck. For this method, the expected sorted order
        /// is by suit, then numerical by face value.
        /// </summary>
        [Test]
		public void TestSortedDeck()
		{
			//Arrange
			DeckManipulator deckMan = new DeckManipulator();
			Card card1 = new Card() { suit = CardSuit.Spade, faceValue = CardValue.Ace };
			Card card2 = new Card() { suit = CardSuit.Spade, faceValue = CardValue.Jack };
			Card card3 = new Card() { suit = CardSuit.Club, faceValue = CardValue.Two };
			Card card4 = new Card() { suit = CardSuit.Heart, faceValue = CardValue.Queen };

			//Act
			List<Card> orderedList = new List<Card> { card1, card2, card3, card4 };
			List<Card> unorderedList = new List<Card> { card4, card1, card2, card3 };
			List<Card> sorted = new List<Card>(deckMan.SortDeck(unorderedList));

			//Assert
			Assert.AreEqual(orderedList, sorted);
		}
        /// <summary>
        /// Tests the sorted deck when the size of the list passed in is 1.
        /// </summary>
		[Test]
		public void TestSortedDeckSize1()
		{
			//Arrange
			DeckManipulator deckMan = new DeckManipulator();

			//Act
			List<Card> listOfOne = new List<Card> { new Card() { suit = CardSuit.Heart, faceValue = CardValue.Ace, } };
			List<Card> sortedListOfOne = deckMan.SortDeck(listOfOne);

			//Assert
			Assert.AreEqual(listOfOne, sortedListOfOne);
		}
        /// <summary>
        /// Tests that the method returns an empty list when given one.
        /// </summary>
		[Test]
		public void TestSortedDeckEmptyList()
		{
			//Arrange
			DeckManipulator deckMan = new DeckManipulator();

			//Act
			List<Card> emptyList = new List<Card>();

			//Assert
			Assert.IsEmpty(deckMan.SortDeck(emptyList));
		}
		/// <summary>
		/// Tests that the method throws a NullReferenceException with the
		/// appropriate error message.
		/// </summary>
		[Test] 
		public void TestSortedDeckNullParam()
		{
			//Arrange
			DeckManipulator deckMan = new DeckManipulator();

			//Act
			List<Card> nullList = null;

			//Assert
			var exception = Assert.Throws<NullReferenceException>(() => deckMan.SortDeck(nullList));
			Assert.That(exception.Message.Contains("Error: Cannot sort a null object"));

		}
		//SortedDeck() tests--------------------------------------------------
		/// <summary>
		/// Tests the sorted deck. For this method, the expected sorted order
		/// is numerical, regardless of suit.
		/// </summary>
		[Test]
		public void TestSortedSuit()
		{
			//Arrange
			DeckManipulator deckMan = new DeckManipulator();
			Card card1 = new Card() { suit = CardSuit.Spade, faceValue = CardValue.Ace };
			Card card2 = new Card() { suit = CardSuit.Spade, faceValue = CardValue.Jack };
			Card card3 = new Card() { suit = CardSuit.Club, faceValue = CardValue.Two };
			Card card4 = new Card() { suit = CardSuit.Heart, faceValue = CardValue.Queen };

			//Act
			List<Card> orderedList = new List<Card> { card1, card3, card2, card4 };
			List<Card> unorderedList = new List<Card> { card4, card1, card2, card3 };
			List<Card> sorted = new List<Card>(deckMan.SortSuit(unorderedList));

			//Assert
			Assert.AreEqual(orderedList, sorted);
		}
        /// <summary>
        /// Tests the sorted suit when the size is 1. 
        /// </summary>
		[Test]
		public void TestSortedSuitSize1()
		{
			//Arrange
			DeckManipulator deckMan = new DeckManipulator();

			//Act
			List<Card> listOfOne = new List<Card> { new Card() { suit = CardSuit.Heart, faceValue = CardValue.Ace, } };
			List<Card> sortedListOfOne = deckMan.SortSuit(listOfOne);

			//Assert
			Assert.AreEqual(listOfOne, sortedListOfOne);
		}
        /// <summary>
        /// Tests the sorted suit when the given list is empty. Expected result
        /// is an empty list.
        /// </summary>
		[Test]
		public void TestSortedSuitEmptyList()
		{
			//Arrange
			DeckManipulator deckMan = new DeckManipulator();

			//Act
			List<Card> emptyList = new List<Card>();

			//Assert
			Assert.IsEmpty(deckMan.SortSuit(emptyList));
		}
        /// <summary>
        /// Tests that a NullReferenceException is thrown with the appropriate
        /// error message when the param pass in is null.
        /// </summary>
		[Test] 
		public void TestSortedSuitNullParam()
		{
			//Arrange
			DeckManipulator deckMan = new DeckManipulator();

			//Act
			List<Card> nullList = null;

			//Assert
			var exception = Assert.Throws<NullReferenceException>(() => deckMan.SortSuit(nullList));
			Assert.That(exception.Message.Contains("Error: Cannot sort a null object"));

		}
		//PrintDeck() tests--------------------------------------------------
		/// <summary>
        /// Tests that an ArgumentException is thrown with the appropriate 
        /// error message when the param passed in is empty.
        /// </summary>
        [Test]
		public void TestPrintDeckEmptyList()
		{
			//Arrange
			DeckManipulator deckMan = new DeckManipulator();

			//Act
			List<Card> emptyList = new List<Card>();

			//Assert
			var exception = Assert.Throws<ArgumentException>(() => deckMan.PrintDeck(emptyList));
			Assert.That(exception.Message.Contains("Error: Cannot print an empty deck."));
		}
		/// <summary>
		/// Tests that a NullReferenceException is thrown with the appropriate 
		/// error message when the param passed in is null.
		/// </summary>
		[Test] 
		public void TestPrintDeckNullParam()
		{
			//Arrange
			DeckManipulator deckMan = new DeckManipulator();

			//Act
			List<Card> nullList = null;

			//Assert
			var exception = Assert.Throws<NullReferenceException>(() => deckMan.PrintDeck(nullList));
			Assert.That(exception.Message.Contains("Error: Cannot print a null object"));

		}
	}
}