﻿/******************************************************************************
 * Program: Card Challenge
 * Class:   CardValue
 * Author:  Sarah Alvarado
 * Date:    May 26, 2017
 * About:   The CardValue class is an enum of face values for a particular deck
 *          of cards. This project is built around a standard deck of 52 cards
 *          where the Ace is the lowest value, and the King is the highest.
 ******************************************************************************/
using System;
namespace CardChallenge
{
    public enum CardValue
    {
        Ace = 1,
        Two = 2,
        Three = 3,
        Four = 4,
        Five = 5,
        Six = 6,
        Seven = 7,
        Eight = 8,
        Nine = 9,
        Ten = 10,
        Jack = 11,
        Queen = 12,
        King = 13,
    }
}
