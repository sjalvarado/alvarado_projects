﻿/******************************************************************************
 * Program: Card Challenge
 * Class:   CardSuit
 * Author:  Sarah Alvarado
 * Date:    May 26, 2017
 * About:   This class is an enum of suits. The suits are ordered the same way
 *          a brand new deck of physical cards would come from the manufacturer.
 ******************************************************************************/
using System;

namespace CardChallenge
{
    public enum CardSuit
    {
        Spade,
        Diamond,
        Club,
        Heart,
    }
}
