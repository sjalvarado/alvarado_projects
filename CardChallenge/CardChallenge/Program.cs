﻿/******************************************************************************
 * Program: Card Challenge
 * Class:   Main
 * Author:  Sarah Alvarado
 * Date:    May 26, 2017
 * About:   This is the main class that runs the project. It prints an original
 *          list, the shuffled list, and the sorted list to the console.
 ******************************************************************************/
using System;
using System.Collections.Generic;

namespace CardChallenge
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            DeckManipulator deckMan = new DeckManipulator();

            //Build the initial deck and print it to the console.
            List<Card> deck = deckMan.BuildDeck();
            Console.WriteLine("Build Deck***********************************");
            deckMan.PrintDeck(deck); 
            Console.WriteLine("\n");

            //Shuffle the deck and print it
            Console.WriteLine("\nShuffled Deck***********************************");
            deckMan.ShuffleDeck(deck); 
            deckMan.PrintDeck(deck); 
            Console.WriteLine("\n");

            //Sort the deck and print it.
            Console.WriteLine("\nSorted Deck***********************************");
            List<Card> sorted = new List<Card>(deckMan.SortDeck(deck)); 
            deckMan.PrintDeck(sorted);
            Console.WriteLine("\n");
        }
    }
}

