﻿/******************************************************************************
 * Program: Card Challenge
 * Class:   DeckManipulator
 * Author:  Sarah Alvarado
 * Date:    May 26, 2017
 * About:   The DeckManipulator class houses the primary functions and logic for
 *          the CardChallenge project. 
 ******************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;

namespace CardChallenge
{
    public class DeckManipulator
    {
        public DeckManipulator()
        {
        }
        /// <summary>
        /// Builds a deck of 'cards' based on the CardSuit and CardValue
        /// ENUMs. 
        /// </summary>
        /// <returns>A List of Cards or 'deck' each with a suit and a 
        /// value.</returns>
        public List<Card> BuildDeck()
        {
            List<Card> deck = new List<Card>();

            for (int i = 0; i < Enum.GetNames(typeof(CardSuit)).Length; i++)
            {
                CardSuit currentSuit = (CardSuit)i;

                for (int j = 1; j <= Enum.GetNames(typeof(CardValue)).Length; j++)
                {
                    deck.Add(new Card { suit = currentSuit, faceValue = (CardValue)j });
                }
            }
            return deck;
        }
        /// <summary>
        /// Shuffles the deck using a random number generator and a basic 
        /// Fisher-Yates shuffle. This algorithm shuffles in place, which is
        /// memory efficient and is proportional to n, the number of items in 
        /// the list.
        /// </summary>
        /// <returns>A randomly shuffled deck.</returns>
        /// <param name="deck">A list of cards to shuffle.</param>
        public List<Card> ShuffleDeck(List<Card> deck)
        {
            if (deck == null)
            {
                throw new NullReferenceException("Error: Cannot shuffle a null object");
            }
            if (deck.Count <= 1)
            {
                return deck;
            }

            Random rand = new Random();

            for (int i = deck.Count - 1; i >= 0; i--)
            {
                int j = rand.Next(0, i);
                Card temp = deck[i];
                deck[i] = deck[j];
                deck[j] = temp;
            }
            return deck;
        }
        /// <summary>
        /// Sorts the deck using a bucket sort to first sort the suits, then
        /// a basic insertion sort to sort each of the suits, then returns
        /// a new sorted list.
        /// </summary>
        /// <returns>A List of cards, sorted ascending by suit, then by face value.</returns>
        /// <param name="deck">The deck to be sorted.</param>
        public List<Card> SortDeck(List<Card> deck)
        {
            if (deck == null)
            {
                throw new NullReferenceException("Error: Cannot sort a null object");
            }
            if (deck.Count <= 1)
            {
                return deck;
            }

            List<List<Card>> buckets = new List<List<Card>>();
            List<Card> sortedDeck = new List<Card>();

            //Create an initialize a new 'bucket' for every suit
            for (int i = 0; i < Enum.GetNames(typeof(CardSuit)).Length; i++)
            {
                buckets.Add(new List<Card>());
            }

            //Sort all cards into the correct 'bucket' by suit
            for (int i = 0; i < deck.Count; i++)
            {
                int thisSuit = (int)deck[i].suit;
                buckets[thisSuit].Add(deck[i]);
            }

            //Perform insertion sort on all lists in buckets
            for (int i = 0; i < buckets.Count; i++)
            {
                buckets[i] = SortSuit(buckets[i]);
            }

            //Concat all buckets to create a master List
            for (int i = 0; i < buckets.Count; i++)
            {
                sortedDeck = sortedDeck.Concat(buckets[i]).ToList();
            }
            return sortedDeck;
        }

        /// <summary>
        /// Method that performs an insertion sort on a specific list. Insertion
        /// sort was chosen because of the size of the data set. When the data 
        /// set is small, insertion sort often out performs other, more complex
        /// sorts.
        /// </summary>
        /// <returns>A list sorted ascending by card face value.</returns>
        /// <param name="fullSuit">Full suit.</param>
        public List<Card> SortSuit(List<Card> fullSuit)
        {
            if (fullSuit == null)
            {
                throw new NullReferenceException("Error: Cannot sort a null object");
            }
            if (fullSuit.Count <= 1)
            {
                return fullSuit;
            }

            int j = 0;
            for (int i = 1; i < fullSuit.Count; i++)
            {
                Card temp = fullSuit[i];
                j = i - 1;
                while (j >= 0 && ((int)fullSuit[j].faceValue > (int)temp.faceValue))
                {
                    fullSuit[j + 1] = fullSuit[j];
                    j -= 1;
                }
                fullSuit[j + 1] = temp;
            }

            return fullSuit;
        }

        /// <summary>
        /// Prints the entire deck to the console. For each card, the face value
        /// and the Unicode value of the suit is displayed. 
        /// </summary>
        /// <param name="deck">A list of Cards to print.</param>
        public void PrintDeck(List<Card> deck)
        {
            if (deck == null)
            {
                throw new NullReferenceException("Error: Cannot print a null object");
            }
            if (deck.Count == 0)
            {
                throw new ArgumentException("Error: Cannot print an empty deck.");
            }
            foreach (Card c in deck)
            {
                string card = null;
                switch (c.suit.ToString())
                {
                    case "Heart":
                        card += c.faceValue.ToString() + '\u2665';
                        break;
                    case "Club":
                        card += c.faceValue.ToString() + '\u2663';
                        break;
                    case "Spade":
                        card += c.faceValue.ToString() + '\u2660';
                        break;
                    case "Diamond":
                        card += c.faceValue.ToString() + '\u2666';
                        break;
                    default:
                        card += c.faceValue.ToString() + c.suit.ToString();
                        break;
                }
                //Format the output so that there are as many lines as there
                //are suits.
                if (deck.IndexOf(c) % (deck.Count / Enum.GetNames(typeof(CardSuit)).Length) == 0)
                    Console.Write("\n" + card);
                else
                {
                    Console.Write(card);
                }
            }
        }

    }
}

