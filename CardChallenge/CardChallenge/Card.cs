﻿/******************************************************************************
 * Program: Card Challenge
 * Class:   Card
 * Author:  Sarah Alvarado
 * Date:    May 26, 2017
 * About:   The Card class is a model for a single card and has 2 properties, 
 *          an Enum CardValue and an Enum CardSuit.
 ******************************************************************************/
using System;

namespace CardChallenge
{
    public class Card
    {
        //The numeric rank of the card   
        public CardValue faceValue { get; set; }

        //The suit of the card
        public CardSuit suit { get; set; }

    }
}
