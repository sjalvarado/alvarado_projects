import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
/*
CS 262 Haskell, Final Project
This project calculates change based on user input at the time that the file is
run on the cmd line. For example, an input of 1.23 would yield [1.23,100,10,10,1,1,1].
The assignment for this project is to refactor Haskell code into Java code using no
side effects.
@author Sarah Alvarado
*/

public class ChangeCalculator {

	//List of denominations to use when making change.
	private static final double[] denominations = new double[] {100,50,25,10,5,1};

	//ChangeCalculator class constructor.
	public ChangeCalculator(String[] amounts){
		double[] thisList = new double[amounts.length];

		try{
			thisList = readAmounts(amounts); //convert the array of Strings to array of doubles
		}
		catch(NumberFormatException e){ //Catch the exception for invalid input
			System.out.println("Invalid input. Please try again with a dollar amount. No symbols please.");
		}

		ArrayList<ArrayList<Double>> masterList = calculateChange(thisList);

		for(ArrayList<Double> d: masterList){ //Verify that the amounts in the master list total the input amount.
			if(calculateSum(d) == true){
				System.out.println(d);
			}
		}
	}

	//Private method that converts the input array of Strings to a list of doubles.
	private double[] readAmounts(String[] input){

		double[] inputAmounts = new double[input.length];//initialize new array with the same number of elements as the method parameter.

		for(int i = 0; i < input.length; i++){
			inputAmounts[i] = 100 * Double.parseDouble(input[i]); //Each amount will be entered as a decimal,
			//so it needs to be converted to a whole number so that it can be partitioned into the correct denominations later.
		}
		return inputAmounts; //return an array of doubles that holds new amounts to be converted to change.

	}

	/*Private method that takes in an array of doubles and returns an ArrayList of ArrayLists that contain the correct
	demoninations for every input amount.
	@param array of type double
	@return ArrayList of ArrayLists of type double
	*/
	private ArrayList<ArrayList<Double>> calculateChange(double[] amounts){

		ArrayList<ArrayList<Double>> masterList = new ArrayList<ArrayList<Double>>();

		if (amounts == null){
			throw new NullPointerException();
		}

		for(int i = 0; i < amounts.length; i++){ //for every input amount, add a list of demoninations
			masterList.add(changeHelper(0, amounts[i]));
		}

		return masterList;

	}

	//Returns an ArrayList of denominations for one input amount.
	private ArrayList<Double> changeHelper(int counter, double amt){

		ArrayList<Double> changeList = new ArrayList<Double>();
		changeList.add(amt / 100); //When master list is printed out, the first element will be the original input amount.
		changeHelper(counter, amt, changeList);

		return changeList; //Returns an ArrayList of demoninations for one input amount.

	}

	//Recursive method that adds all denominations to an ArrayList and returns them
	//to the first changeHelper method.
	private void changeHelper(int count, double amount, ArrayList<Double> changeList){
		int counter = count;
		System.out.println("Made it here " + counter);
		if (amount == 0){
			return;
		}
		else if (counter > denominations.length){ //if the counter is larger than the list of demoninations, return
			return;
		}
		else if (calculate(denominations[counter], amount) != 0){ //If the demonination located at the index of the counter
			changeList.add(denominations[counter]);               //is greater than or equal to the amount, then add the demonination
			amount = amount - denominations[counter];             //to the list of change to be given.
			changeHelper(counter, amount, changeList);
		}
		else {
			counter++;                                            //Counter is incremented because amount is smaller than current
			changeHelper(counter, amount, changeList);            //value of the demonination located at the index of the counter.
		}                                                         //Move on to the next demonination and run the method again.
	}

	//Returns the denomination if the amount is greater than or equal to it. Otherwise, return 0.
	private double calculate (double denom, double amount){
		while(amount >= denom){
			return denom;
		}
		return 0;
	}

	//Method to check the correctness of the change to give.
	private boolean calculateSum (ArrayList<Double> changeList){
		double sum = 0;
		//add up everything in the list
		for(double d: changeList){
			sum = sum + d;
		}
		//remove the first element of the list and round the amount
		sum = Math.round(sum - changeList.get(0));

		//return true if the new sum is equal to the first element of the list
		if (sum / 100  == changeList.get(0)){
			return true;
		}
		return false;
	}


	public static void main (String[] args){
		ChangeCalculator cc = new ChangeCalculator(args);

	}
}
